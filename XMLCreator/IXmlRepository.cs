﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLCreator.Models;

namespace XMLCreator
{
    public interface IXmlRepository
    {
        void Save(Catalog catalog);
        FileStream Load();
    }
}
