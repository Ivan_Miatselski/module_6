﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using XMLCreator.Models;

namespace XMLCreator
{
    public class XmlRepository : IXmlRepository
    {
        private string FilePath { get; set; }

        public XmlRepository(string filePath)
        {
            if (String.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException();    
            }

            this.FilePath = filePath;
        }

        public FileStream Load()
        {
            FileStream fileStream;
            try
            {
                fileStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite);
            }
            catch (IOException)
            {
                throw new IOException($"Cannot load {FilePath} this file");
            }

            return fileStream;
        }

        public void Save(Catalog catalog)
        {
            if (catalog == null)
            {
                throw new ArgumentNullException();    
            }

            try
            {
                using (var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write))
                {                  
                    var serializer = new XmlSerializer(typeof(Catalog));
                    serializer.Serialize(fileStream, catalog);
                }
            }
            catch (IOException)
            {
                throw new IOException($"Cannot save current data");
            }
        }
    }
}
