﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XMLCreator.Models
{
    [XmlRoot(IsNullable = false)]
    public class Catalog
    {
        [XmlAttribute]
        public string Library { get; set; }

        [XmlAttribute]
        public DateTime DateCreated { get; set; }

        [XmlElement("Book")]
        public List<Book> books { get; set; }

        [XmlElement("Magazin")]
        public List<Magazin> Magazins { get; set; }

        [XmlElement("Patent")]
        public List<Patent> Patents { get; set; }
    }
}
