﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XMLCreator.Models
{
    [XmlRoot]
    public class Patent : IEntity
    {
        [XmlElement(IsNullable = false)]
        public string Title { get; set; }

        [XmlElement]
        public List<string> Creators { get; set; }

        [XmlElement]
        public string Country { get; set; }

        [XmlElement(IsNullable = false)]
        public int RegisterNumber { get; set; }

        [XmlElement]
        public DateTime DateRequest { get; set; }

        [XmlElement]
        public DateTime DatePublication { get; set; }

        [XmlElement(IsNullable = false)]
        public int CountPage { get; set; }

        [XmlElement]
        public string Annotation { get; set; }

        [XmlAttribute]
        public string Description { get; set; }
    }
}
