﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLCreator.Models
{
    public interface IEntity
    {
        string Description { get; set; }
    }
}
