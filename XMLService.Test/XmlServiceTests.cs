﻿using System;
using System.IO;
using NUnit.Framework;
using System.Reflection;
using XMLCreator.Models;
using System.Collections.Generic;

namespace XMLService.Test
{
    [TestFixture]
    public class XmlServiceTests
    {
        [TestCase(null, null, typeof(IOException))]
        public void CreatingService_RepositoryNotFound_Exception(string filePath, Catalog catalog, Type exception)
        {
            Assert.Throws(exception, () => new XmlService());
        }

        //Should be created repository
        [TestCase("C:/Users/Ivan_Miatselski2/Desktop/module_6/ConsoleApplication/bin/Debug/q.xml")]
        public void AddNewValueToRepository_SuccessAddedNewItem(string repositoryName)
        {
            XmlService service = new XmlService(repositoryName);
            bool operationResult = service.AddEntity(new Book() { Id = 3, Title = "reeew", City = "Minsk", Annotation = "qwsssa", Authors = new List<string> { "Ivan" }, Publishing = "ds", Description = "Book number 3" });
            Assert.AreEqual(operationResult, true);
        }

        //Should be created repository
        [TestCase("C:/Users/Ivan_Miatselski2/Desktop/module_6/ConsoleApplication/bin/Debug/q.xml", typeof(ArgumentNullException))]
        public void AddNewValueToRepository_FailedAddedNewItem(string repositoryName, Type exception)
        {
            XmlService service = new XmlService(repositoryName);
            Assert.Throws(exception, () => service.AddEntity(null));
        }
    }
}


