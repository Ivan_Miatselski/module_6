﻿using System;
using System.IO;
using NUnit.Framework;
using XMLCreator;
using XMLCreator.Models;

namespace XMLService.Test
{
    [TestFixture]
    public class XmlRepositoryTest
    {
        [TestCase(null, typeof(ArgumentException))]
        [TestCase("", typeof(ArgumentException))]
        public void CreateRepositoryWithoutPath_Exception(string path, Type exception)
        {
            Assert.Throws(exception, () => new XmlRepository(path));
        }

        [TestCase("qwerty.xml", typeof(IOException))]
        public void GetStreamToRepository_RepositoryNotExsist_Exception(string repositoryName, Type exception)
        {
            XmlRepository repository = new XmlRepository(repositoryName);
            Assert.Throws(exception, () => repository.Load());
        }

        //Should be created repository
        [TestCase("C:/Users/Ivan_Miatselski2/Desktop/module_6/ConsoleApplication/bin/Debug/q.xml")]
        public void GetStreamToRepository_ReturnStreamToRepository(string path)
        {
            XmlRepository repository = new XmlRepository(path);
            Stream stream = repository.Load();
            Assert.AreNotEqual(stream, null);
        }

        //Should be created repository
        [TestCase("C:/Users/Ivan_Miatselski2/Desktop/module_6/ConsoleApplication/bin/Debug/q.xml", null, typeof(ArgumentNullException))]
        public void SaveDataInRepository_Exception(string path, Catalog catalog, Type exception)
        {
            XmlRepository repository = new XmlRepository(path);
            Assert.Throws(exception, () => repository.Save(catalog));
        }
    }
}
